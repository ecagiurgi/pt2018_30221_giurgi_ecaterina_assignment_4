package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bll.Bank;
import modell.Account;
import modell.AccountType;
import modell.Person;

class BankTest {
	 private Bank bank;
	 
	 public BankTest() {
		 this.bank = new Bank();
	 }

	@Test
	void addPerson() {
		Assertions.assertEquals(0,bank.getAccountTable().size());
		bank.addPerson(123456,"Eca");
		Assertions.assertEquals(1,bank.getAccountTable().size());
	}

	@Test
	void removePerson() {
		bank.addPerson(123456,"Eca");
		Assertions.assertEquals(1,bank.getAccountTable().size());
		bank.removePerson(123456);
		Assertions.assertEquals(0,bank.getAccountTable().size());
	}
	
	@Test
	void addAccount() {
		bank.addPerson(123456,"Eca");
		bank.addAccount(123456, AccountType.SAVING_ACCOUNT);
		Account account = bank.getAccountTable().values().iterator().next().get(0);
		Assertions.assertEquals(AccountType.SAVING_ACCOUNT, account.getType());
		Assertions.assertEquals(123456, account.getMainHolder().getCnp());

	}
	
	@Test
	void removeAccount() {
		bank.addPerson(123456,"Eca");
		bank.addAccount(123456, AccountType.SAVING_ACCOUNT);
		Account account = bank.getAccountTable().values().iterator().next().get(0);
		bank.removeAccount(account.getAccountId());
		Assertions.assertTrue(bank.getAccountTable().values().iterator().next().isEmpty());  // verifica daca lista de account e goala

	}
	
	@Test
	void readWriteDataAccountTest() {
		bank.addPerson(123456,"Eca");
		bank.addAccount(123456, AccountType.SAVING_ACCOUNT);
		Account account = bank.getAccountTable().values().iterator().next().get(0);
		Person person = account.getMainHolder();
		bank.writeAccountData();
		Hashtable<Person, List<Account>> deserializedAccountData = bank.readAccountData();
		Account deserializedAccount = deserializedAccountData.values().iterator().next().get(0);
		Person deserializedPerson = deserializedAccount.getMainHolder();
		Assertions.assertEquals(account.getAccountId(), deserializedAccount.getAccountId());
		Assertions.assertEquals(person.getCnp(), deserializedPerson.getCnp());

	}
	
	@Test
	void reportTest() throws IOException {
		bank.addPerson(123456,"Eca");
		bank.addAccount(123456, AccountType.SAVING_ACCOUNT);
		Account account = bank.getAccountTable().values().iterator().next().get(0);
		account.deposit(100);
		account.withdraw(50);
		bank.reportGenerator(123456);
		List<String> lines = Files.readAllLines(Paths.get("person123456.txt"));
		Assertions.assertEquals(2,lines.size());  // citim din fisier si verific daca is 2 linii 

	}
	
	
}

	

	
	


