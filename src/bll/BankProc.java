package bll;

import java.util.Hashtable;
import java.util.List;

import modell.Account;
import modell.AccountType;
import modell.Person;

public interface BankProc {
	/**
	 * Add Person to the Hashtable.
	 * 
	 * @param cnp Personal Identification Number.
	 * @param nume Name of the person. 
	 */
	void addPerson(long cnp, String nume);
	
	/**
	 * Remove Person from the Hashtable.
	 * 
	 * @param cnp Personal Identification Number.
	 */
	void removePerson(long cnp);
	
	/**
	 * Add Account to a Person.
	 * 
	 * @param cnp Personal Identification Number of the person.
	 * @param type Type of the account.
	 */
	void addAccount(long cnp, AccountType type);
	
	/**
	 * Remove Account from the Person.
	 * 
	 * @param accountId Account-Id.
	 */
	void removeAccount(String accountId);
	
	/**
	 * Save serialized hashtable.
	 */
	void writeAccountData();
	
	/**
	 * Load serialized hashtable.
	 * 
	 * @return the hashtable.
	 */
	Hashtable<Person,List <Account>> readAccountData();
	
	/**
	 * Generate report of operations. 
	 * 
	 * @param cnp Personal Identification Number of the person.
	 */
	void reportGenerator(long cnp);
		

}
