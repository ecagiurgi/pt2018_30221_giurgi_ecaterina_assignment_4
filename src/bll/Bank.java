package bll;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import modell.Account;
import modell.AccountType;
import modell.Person;
import modell.SavingAccount;
import modell.SpendingAccount;

public class Bank implements BankProc {
	private Hashtable<Person, List<Account>> accountTable;

	public Bank() {
		super();
		hashTableNotNull();
		accountTable = new Hashtable<>();
	}

	public Hashtable<Person, List<Account>> getAccountTable() {
		return accountTable;
	}

	@Override
	public void addPerson(long cnp, String nume) {
		assert cnp != 0 && !nume.isEmpty();
		Person person = new Person(cnp, nume);
		accountTable.put(person, new ArrayList<>());
		hashTableNotNull();
		assert !accountTable.isEmpty();
	}

	@Override
	public void removePerson(long cnp) {
		assert cnp != 0;
		Set<Person> persons = accountTable.keySet();
		for (Person person : persons) {
			if (cnp == person.getCnp()) {
				accountTable.remove(person);
			}
		}
		hashTableNotNull();
	}

	@Override
	public void addAccount(long cnp, AccountType type) {
		assert cnp != 0 && type != null;
		Set<Person> persons = accountTable.keySet();
		for (Person person : persons) {
			if (cnp == person.getCnp()) {
				if (type.equals(AccountType.SPENDING_ACCOUNT)) {
					accountTable.get(person).add(new SpendingAccount(person));
				} else {
					accountTable.get(person).add(new SavingAccount(person,12));
				}
			}
		}
		hashTableNotNull();
		assert accountTable.size() > 0;
	}

	@Override
	public void removeAccount(String accountId) {
		assert !accountId.isEmpty();
		List<Account> tmpList = null;
		Account tmpAccount = null;

		Collection<List<Account>> listOfAccountsList = accountTable.values();
		for (List<Account> listOfAccounts : listOfAccountsList) {
			for (Account account : listOfAccounts) {
				if (accountId.equals(account.getAccountId())) {
					tmpList = listOfAccounts;
					tmpAccount = account;
				}
			}
		}
		if (tmpList != null && tmpAccount != null) {
			tmpList.remove(tmpAccount);
		}
		hashTableNotNull();
	}

	@Override
	public void writeAccountData() {
		try {
			FileOutputStream fileOut = new FileOutputStream("account.ser"); // face un fisier in caz ca nu exista
			ObjectOutputStream out = new ObjectOutputStream(fileOut); // transforma obiectele in format serializat
			out.writeObject(accountTable);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
		hashTableNotNull();
	}

	@Override
	public Hashtable<Person, List<Account>> readAccountData() {
		Hashtable<Person, List<Account>> tmpHashtable = new Hashtable<Person, List<Account>>();
		try {
			FileInputStream fileIn = new FileInputStream("account.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			tmpHashtable = (Hashtable<Person, List<Account>>) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
		} finally {
			accountTable = tmpHashtable;
		}
		hashTableNotNull();
		return accountTable;
	}

	@Override
	public void reportGenerator(long cnp) {
		assert cnp != 0;
		try {
			Set<Person> persons = accountTable.keySet();
			for (Person person : persons) {
				if (cnp == person.getCnp()) {
					String str = String.join("\n", person.getReports());
					byte[] strToBytes = str.getBytes();
					FileOutputStream outputStream = new FileOutputStream("person" + cnp + ".txt");
					outputStream.write(strToBytes);
					outputStream.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		hashTableNotNull();
	}
	
	private Account findAccountByAccountId(String accountId) {
		Collection<List<Account>> listOfAccountsList = accountTable.values();
		for (List<Account> listOfAccounts : listOfAccountsList) {
			for (Account account : listOfAccounts) {
				if (accountId.equals(account.getAccountId())) {
					return account;
					
				}
				
			}
		}
		return null;
		
	}
	
	public void deposit(double amount,String accountId) {
		Account account = findAccountByAccountId(accountId);
		if(account!= null) {
			account.deposit(amount);
		}
	}
	
	public void withdraw(double amount,String accountId) {
		Account account = findAccountByAccountId(accountId);
		if(account!= null) {
			account.withdraw(amount);
		}
	}
	
	public void hashTableNotNull(){
		assert accountTable != null;
	}
}
