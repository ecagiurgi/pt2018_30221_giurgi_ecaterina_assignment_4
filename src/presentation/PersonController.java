package presentation;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import bll.Bank;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import modell.Person;

public class PersonController implements Initializable {

	@FXML
	private TableView<PersonTableData> personTable;

	@FXML
	private TableColumn<PersonTableData, String> nameColumn;

	@FXML
	private TableColumn<PersonTableData, String> cnpColumn;

	@FXML
	private TextField nameTF;

	@FXML
	private TextField cnpTF;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button removeButton;

	@FXML
	private Button generateReportButton;
	
	private ObservableList<PersonTableData> data = FXCollections.observableArrayList();

	private Bank bank;

	public PersonController(Bank bank) {
		this.bank = bank;
	}


	public void addPerson() {
		
		 String name = nameTF.getText(); 
		 long cnp = Long.parseLong(cnpTF.getText());
		 bank.addPerson(cnp, name);
		 refresh();
	 }


	public void removePerson() {

		PersonTableData selectedItem = personTable.getSelectionModel().getSelectedItem(); 
		if (selectedItem != null) {
			bank.removePerson(Long.parseLong(selectedItem.getCnp()));
			refresh();
			 
		}
		
	}
	
	public void generateReportPerson() {

		PersonTableData selectedItem = personTable.getSelectionModel().getSelectedItem(); 
		if (selectedItem != null) {
			bank.reportGenerator(Long.parseLong(selectedItem.getCnp()));
			refresh();
			 
		}
		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		populate();
		initColumns();
		initButtons();
	    personTable.setItems(data);
	}

	private void initButtons() {
		addButton.setOnAction((event) -> this.addPerson());
		removeButton.setOnAction((event) -> this.removePerson());
		generateReportButton.setOnAction((event) -> this.generateReportPerson());
	}

	private void populate() {
		Set<Person> persons = bank.getAccountTable().keySet();
		List<PersonTableData> personsTableData = new ArrayList<>();
		for (Person p : persons) {
			personsTableData.add(new PersonTableData(p.getCnp(), p.getNume()));
		}
		data.addAll(FXCollections.observableArrayList(personsTableData));
	}
	
	private void refresh() {
		data.clear();
		populate();
	}
	
	private void initColumns() {
		
		nameColumn.setText("Name");
		nameColumn.setCellValueFactory(cell -> cell.getValue().nameProperty());
		nameColumn.setCellFactory(TextFieldTableCell.<PersonTableData>forTableColumn());
		
		cnpColumn.setText("Cnp");
		cnpColumn.setCellValueFactory(cell -> cell.getValue().cnpProperty());
		cnpColumn.setCellFactory(TextFieldTableCell.<PersonTableData>forTableColumn());
	
	}

}
