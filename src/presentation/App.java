package presentation;

import bll.Bank;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class App extends Application {

	private Bank bank;
	
	public App() {
		bank = new Bank();
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		bank.readAccountData();
		
		TabPane tabPane = new TabPane();
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Main.fxml"));  
		AnchorPane root = (AnchorPane)fxmlLoader.load();     
		
		FXMLLoader personFXMLLoader = new FXMLLoader(getClass().getResource("PersonTabPane.fxml"));  
		PersonController personController = new PersonController(bank);
		personFXMLLoader.setController(personController);
		AnchorPane loadedPersonTab = personFXMLLoader.load();
		Tab personTab = new Tab("Persons");
		personTab.setContent(loadedPersonTab);
		tabPane.getTabs().add(personTab);
		
		FXMLLoader accountFXMLLoader = new FXMLLoader(getClass().getResource("AccountTabPane.fxml"));  
		AccountController accountController = new AccountController(bank);
		accountFXMLLoader.setController(accountController);
		AnchorPane loadedAccountTab = accountFXMLLoader.load();
		Tab accountTab = new Tab("Accounts");
		accountTab.setContent(loadedAccountTab);
		tabPane.getTabs().add(accountTab);
		
		root.getChildren().add(tabPane);
		
		Scene scene = new Scene(root); 
		stage.setScene(scene);    
		stage.show();   
		stage.setOnCloseRequest(event -> {
			bank.writeAccountData();
		});
		
		

	}

	public static void main(String[] args) {
		launch(args);
	}

}
