package presentation;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleStringProperty;

public class PersonTableData {
	
	private SimpleStringProperty cnp;
	private SimpleStringProperty name;

	public PersonTableData(long cnp, String name) {
		this.cnp = new SimpleStringProperty(String.valueOf(cnp));
		this.name = new SimpleStringProperty(name);
	}

	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}
	
	public String getCnp() {
		return cnp.get();
	}

	public void setCnp(String cnp) {
		this.cnp.set(cnp);
	}

	public SimpleStringProperty nameProperty() {
		return name;
	}
	
	public SimpleStringProperty cnpProperty() {
		return cnp;
	}

	
	
}
