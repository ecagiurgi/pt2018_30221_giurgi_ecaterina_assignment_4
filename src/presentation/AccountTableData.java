package presentation;

import javafx.beans.property.SimpleStringProperty;
import modell.AccountType;
import modell.Person;

public class AccountTableData {
	private SimpleStringProperty accountId;
	private SimpleStringProperty balance;
	private SimpleStringProperty mainHolder;
	private SimpleStringProperty type;
	
	public AccountTableData(String accountId, double balance, Person mainHolder,AccountType type) {
		this.accountId= new SimpleStringProperty(accountId);
		this.balance=new SimpleStringProperty(String.valueOf(balance));
		this.mainHolder= new SimpleStringProperty(String.valueOf(mainHolder.getCnp()));
		this.type=new SimpleStringProperty(type.toString());
	}
	
	public SimpleStringProperty accountIdProperty() {
		return accountId;
	}
	
	public SimpleStringProperty balanceProperty() {
		return balance;
	}
	
	public SimpleStringProperty mainHolderProperty() {
		return mainHolder;
	}
	public SimpleStringProperty typeProperty() {
		return type;
	}
}
