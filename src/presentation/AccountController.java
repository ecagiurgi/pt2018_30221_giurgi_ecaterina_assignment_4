package presentation;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import bll.Bank;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import modell.Account;
import modell.AccountType;
import modell.Person;

public class AccountController implements Initializable{
	
	
	
	
	@FXML
    private TableView<AccountTableData> accountTable;
	
	@FXML
	private TableColumn<AccountTableData, String> accountIdColumn; 
	
	@FXML
	private TableColumn<AccountTableData, String> balanceColumn;
	

	@FXML
	private TableColumn<AccountTableData, String> mainHolderColumn; 
	
	@FXML
	private TableColumn<AccountTableData, String> typeColumn;
	
	@FXML
	private ComboBox <String> mainHolderCbo;
	
	@FXML
	private ComboBox <String> accountTypeCbo;

	@FXML
	private TextField amountTF;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button removeButton;
	
	@FXML
	private Button depositButton;
	
	@FXML
	private Button withdrawButton;
	
	
	private ObservableList<AccountTableData> data = FXCollections.observableArrayList();
	private Bank bank; 
	
	public AccountController(Bank bank) {
		this.bank = bank;
	}

	@FXML
	public void addAccount() {
		 long cnp = Long.parseLong(mainHolderCbo.getSelectionModel().getSelectedItem());
		 AccountType type = AccountType.valueOf(accountTypeCbo.getSelectionModel().getSelectedItem());
		 bank.addAccount(cnp,type);
		 refresh();
	}
	
	@FXML
	public void removeAccount() {
		AccountTableData selectedItem =accountTable.getSelectionModel().getSelectedItem(); 
		if (selectedItem != null) {
			bank.removeAccount(selectedItem.accountIdProperty().get());
			refresh();
		}
	}

	@FXML
	public void depositAccount() {
		double amount=Double.parseDouble(amountTF.getText());
		AccountTableData selectedItem =accountTable.getSelectionModel().getSelectedItem(); 
		if (selectedItem != null) {
			bank.deposit(amount,selectedItem.accountIdProperty().get());
			refresh();
		}
			
	}

	@FXML
	public void withdrawAccount() {
		double amount=Double.parseDouble(amountTF.getText());
		AccountTableData selectedItem =accountTable.getSelectionModel().getSelectedItem(); 
		if (selectedItem != null) {
			bank.withdraw(amount, selectedItem.accountIdProperty().get());
			refresh();
		}
			
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		bank.readAccountData();
		populate();
		initColumns();
		initComboBoxes();
		initButtons();
	    accountTable.setItems(data);
	}

	private void initButtons() {
		addButton.setOnAction((event)-> this.addAccount());
		removeButton.setOnAction((event) -> this.removeAccount());
		depositButton.setOnAction((event)-> this.depositAccount());
		withdrawButton.setOnAction((event) -> this.withdrawAccount());
	}

	private void initColumns() {
		
		accountIdColumn.setText("AccountId");
		accountIdColumn.setCellValueFactory(cell -> cell.getValue().accountIdProperty());
		accountIdColumn.setCellFactory(TextFieldTableCell.<AccountTableData>forTableColumn());
		
		balanceColumn.setText("Balance");
		balanceColumn.setCellValueFactory(cell -> cell.getValue().balanceProperty());
		balanceColumn.setCellFactory(TextFieldTableCell.<AccountTableData>forTableColumn());
		
		mainHolderColumn.setText("Mainholder");
		mainHolderColumn.setCellValueFactory(cell -> cell.getValue().mainHolderProperty());
		mainHolderColumn.setCellFactory(TextFieldTableCell.<AccountTableData>forTableColumn());
	
		typeColumn.setText("Type");
		typeColumn.setCellValueFactory(cell -> cell.getValue().typeProperty());
		typeColumn.setCellFactory(TextFieldTableCell.<AccountTableData>forTableColumn());
	
	}
	
	private void initComboBoxes() {
		
		Set<Person> persons = bank.getAccountTable().keySet();
		List<String> personCnp = persons.stream().map(person -> String.valueOf(person.getCnp())).collect(Collectors.toList());
		mainHolderCbo.setItems(FXCollections.observableArrayList(personCnp));
		accountTypeCbo.setItems(FXCollections.observableArrayList(Arrays.asList("SPENDING_ACCOUNT","SAVING_ACCOUNT")));

	}
	
	
	
	private void populate() {
		Collection<List<Account>> accountsList = bank.getAccountTable().values();
		List<AccountTableData> accountsTableData = new ArrayList<>();
		for (List<Account> accountList : accountsList) {
			for(Account account : accountList) {
				accountsTableData.add(new AccountTableData(account.getAccountId(), account.getBalance(),account.getMainHolder(),account.getType()));
		}
		}
		data.addAll(FXCollections.observableArrayList(accountsTableData));
	}

	
	private void refresh() {
		data.clear();
		populate();
	}

	
}


