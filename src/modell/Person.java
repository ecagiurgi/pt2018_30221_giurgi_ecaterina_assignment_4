package modell;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable,  Observer{

	private static final long serialVersionUID = -2545882245444196603L;
	private long cnp;
	private String nume;
	private List<String> reports;

	
	public Person(long cnp, String nume) {
		super();
		this.cnp = cnp;
		this.nume = nume;
		this.reports = new ArrayList<String>();
	}
	
	public long getCnp() {
		return cnp;
	}
	public String getNume() {
		return nume;
	}
	
	public List<String> getReports() {
		return reports;
	}

	@Override
	public void update(Observable observable, Object text) {
		if( text instanceof String) {
			String report=(String) text;
			reports.add(report);
		
	}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (cnp ^ (cnp >>> 32));
		result = prime * result + ((nume == null) ? 0 : nume.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (cnp != other.cnp)
			return false;
		if (nume == null) {
			if (other.nume != null)
				return false;
		} else if (!nume.equals(other.nume))
			return false;
		return true;
	}
	
	
	
	
	

}
