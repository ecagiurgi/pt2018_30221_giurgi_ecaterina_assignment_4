package modell;

public class SavingAccount extends Account {

	private static final long serialVersionUID = -973049623683343023L;
	private double interest;
	private int nrOfMonths;

	public SavingAccount(Person mainHolder, int nrOfMonths) {
		super(AccountType.SAVING_ACCOUNT, mainHolder);
		this.nrOfMonths = nrOfMonths;
	}

	@Override
	public void deposit(double amount) {
		if (getBalance() == 0) { // ca sa faca o data doar
			if (nrOfMonths < 12) {
				interest = 0.5;
			} else {
				interest = 1;
			}
			super.deposit(amount);
		}
	}

	public void withdraw() {
		if (getBalance() > 0) { // ca sa faca o data doar
			super.withdraw(getBalance());
		}
	}
}
