package modell;

import java.io.Serializable;
import java.util.Observable;
import java.util.UUID;


public class Account extends Observable implements Serializable {

	private static final long serialVersionUID = -5915257228159510316L;
	private String accountId;
	private double balance;
	private AccountType type;
	private Person mainHolder;
	
	protected Account( AccountType type, Person mainHolder) {
		super();
		this.accountId=UUID.randomUUID().toString();
		this.type = type;
		this.mainHolder = mainHolder;
		addObserver(mainHolder);
	}
	
	public void withdraw(double amount) {
		assert amount <= balance ;
		balance = balance - amount;
		checkBalance();
		setChanged();
		notifyObservers(" withdraw amount: " + amount + 
				" from account with id: " + accountId + 
				" current balance: " + balance);
	}
	
	public void checkBalance() {
		assert balance >= 0;

	}
	
	public void deposit(double amount) {
		assert amount >= 0 ;
		balance = balance + amount;
		checkBalance();
		setChanged();
		notifyObservers(" deposit amount: " + amount + 
				" to account with id: " + accountId + 
				" current balance: " + balance);
	}
	@Override
	public void notifyObservers(Object text) {
		super.notifyObservers(text);
	}
	


	public String getAccountId() {
		return accountId;
	}

	public double getBalance() {
		return balance;
	}

	public AccountType getType() {
		return type;
	}

	public Person getMainHolder() {
		return mainHolder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((mainHolder == null) ? 0 : mainHolder.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
			return false;
		if (mainHolder == null) {
			if (other.mainHolder != null)
				return false;
		} else if (!mainHolder.equals(other.mainHolder))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	

	
	
	
	 
	

}
